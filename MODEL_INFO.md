
Model info for convnextv2_pico.fcmae_ft_in1k
============================================


Sequential (Input shape: 64 x 3 x 224 x 224)
============================================================================
Layer (type)         Output Shape         Param #    Trainable 
============================================================================
                     64 x 64 x 56 x 56   
Conv2d                                    3136       True      
LayerNorm2d                               128        True      
Identity                                                       
Conv2d                                    3200       True      
LayerNorm2d                               128        True      
____________________________________________________________________________
                     64 x 256 x 56 x 56  
Conv2d                                    16640      True      
GELU                                                           
Dropout                                                        
GlobalResponseNorm                        512        True      
____________________________________________________________________________
                     64 x 64 x 56 x 56   
Conv2d                                    16448      True      
Dropout                                                        
Identity                                                       
Identity                                                       
Conv2d                                    3200       True      
LayerNorm2d                               128        True      
____________________________________________________________________________
                     64 x 256 x 56 x 56  
Conv2d                                    16640      True      
GELU                                                           
Dropout                                                        
GlobalResponseNorm                        512        True      
____________________________________________________________________________
                     64 x 64 x 56 x 56   
Conv2d                                    16448      True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm2d                               128        True      
____________________________________________________________________________
                     64 x 128 x 28 x 28  
Conv2d                                    32896      True      
Conv2d                                    6400       True      
LayerNorm2d                               256        True      
____________________________________________________________________________
                     64 x 512 x 28 x 28  
Conv2d                                    66048      True      
GELU                                                           
Dropout                                                        
GlobalResponseNorm                        1024       True      
____________________________________________________________________________
                     64 x 128 x 28 x 28  
Conv2d                                    65664      True      
Dropout                                                        
Identity                                                       
Identity                                                       
Conv2d                                    6400       True      
LayerNorm2d                               256        True      
____________________________________________________________________________
                     64 x 512 x 28 x 28  
Conv2d                                    66048      True      
GELU                                                           
Dropout                                                        
GlobalResponseNorm                        1024       True      
____________________________________________________________________________
                     64 x 128 x 28 x 28  
Conv2d                                    65664      True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm2d                               256        True      
____________________________________________________________________________
                     64 x 256 x 14 x 14  
Conv2d                                    131328     True      
Conv2d                                    12800      True      
LayerNorm2d                               512        True      
____________________________________________________________________________
                     64 x 1024 x 14 x 14 
Conv2d                                    263168     True      
GELU                                                           
Dropout                                                        
GlobalResponseNorm                        2048       True      
____________________________________________________________________________
                     64 x 256 x 14 x 14  
Conv2d                                    262400     True      
Dropout                                                        
Identity                                                       
Identity                                                       
Conv2d                                    12800      True      
LayerNorm2d                               512        True      
____________________________________________________________________________
                     64 x 1024 x 14 x 14 
Conv2d                                    263168     True      
GELU                                                           
Dropout                                                        
GlobalResponseNorm                        2048       True      
____________________________________________________________________________
                     64 x 256 x 14 x 14  
Conv2d                                    262400     True      
Dropout                                                        
Identity                                                       
Identity                                                       
Conv2d                                    12800      True      
LayerNorm2d                               512        True      
____________________________________________________________________________
                     64 x 1024 x 14 x 14 
Conv2d                                    263168     True      
GELU                                                           
Dropout                                                        
GlobalResponseNorm                        2048       True      
____________________________________________________________________________
                     64 x 256 x 14 x 14  
Conv2d                                    262400     True      
Dropout                                                        
Identity                                                       
Identity                                                       
Conv2d                                    12800      True      
LayerNorm2d                               512        True      
____________________________________________________________________________
                     64 x 1024 x 14 x 14 
Conv2d                                    263168     True      
GELU                                                           
Dropout                                                        
GlobalResponseNorm                        2048       True      
____________________________________________________________________________
                     64 x 256 x 14 x 14  
Conv2d                                    262400     True      
Dropout                                                        
Identity                                                       
Identity                                                       
Conv2d                                    12800      True      
LayerNorm2d                               512        True      
____________________________________________________________________________
                     64 x 1024 x 14 x 14 
Conv2d                                    263168     True      
GELU                                                           
Dropout                                                        
GlobalResponseNorm                        2048       True      
____________________________________________________________________________
                     64 x 256 x 14 x 14  
Conv2d                                    262400     True      
Dropout                                                        
Identity                                                       
Identity                                                       
Conv2d                                    12800      True      
LayerNorm2d                               512        True      
____________________________________________________________________________
                     64 x 1024 x 14 x 14 
Conv2d                                    263168     True      
GELU                                                           
Dropout                                                        
GlobalResponseNorm                        2048       True      
____________________________________________________________________________
                     64 x 256 x 14 x 14  
Conv2d                                    262400     True      
Dropout                                                        
Identity                                                       
Identity                                                       
LayerNorm2d                               512        True      
____________________________________________________________________________
                     64 x 512 x 7 x 7    
Conv2d                                    524800     True      
Conv2d                                    25600      True      
LayerNorm2d                               1024       True      
____________________________________________________________________________
                     64 x 2048 x 7 x 7   
Conv2d                                    1050624    True      
GELU                                                           
Dropout                                                        
GlobalResponseNorm                        4096       True      
____________________________________________________________________________
                     64 x 512 x 7 x 7    
Conv2d                                    1049088    True      
Dropout                                                        
Identity                                                       
Identity                                                       
Conv2d                                    25600      True      
LayerNorm2d                               1024       True      
____________________________________________________________________________
                     64 x 2048 x 7 x 7   
Conv2d                                    1050624    True      
GELU                                                           
Dropout                                                        
GlobalResponseNorm                        4096       True      
____________________________________________________________________________
                     64 x 512 x 7 x 7    
Conv2d                                    1049088    True      
Dropout                                                        
Identity                                                       
Identity                                                       
Identity                                                       
____________________________________________________________________________
                     64 x 512 x 1 x 1    
AdaptiveAvgPool2d                                              
AdaptiveMaxPool2d                                              
____________________________________________________________________________
                     64 x 1024           
Flatten                                                        
BatchNorm1d                               2048       True      
Dropout                                                        
____________________________________________________________________________
                     64 x 512            
Linear                                    524288     True      
ReLU                                                           
BatchNorm1d                               1024       True      
Dropout                                                        
____________________________________________________________________________
                     64 x 13             
Linear                                    6656       True      
____________________________________________________________________________

Total params: 9,086,272
Total trainable params: 9,086,272
Total non-trainable params: 0

Optimizer used: <function Adam at 0x7de267c23eb0>
Loss function: FlattenedLoss of CrossEntropyLoss()

Callbacks:
  - TrainEvalCallback
  - CastToTensor
  - Recorder
  - ProgressCallback